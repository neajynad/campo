import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  myDate: any;

  constructor(public navCtrl: NavController) {
    let timeNow = new Date().getDay();
    console.log(timeNow)
  }
  
}
