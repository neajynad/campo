import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  myDate:any
  items: Observable<any[]>;
  constructor(public navCtrl: NavController,private afDB: AngularFireDatabase) {
    afDB.list('BaseDias').valueChanges().subscribe((data)=>{
      console.log(data)
    });
    
  }
  mostrar(){
    console.log(this.myDate)
    console.log(this.items)
    let promesa = this.afDB.list('BaseDias').set("item1",{
      "Prueba1": "Hola"
    });
    promesa.then(()=> console.log("success"));
  }
}
